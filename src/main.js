import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import '@progress/kendo-ui'
import '@progress/kendo-theme-default/dist/all.css'
import {
  Scheduler,
  SchedulerResource,
  SchedulerView,
  SchedulerInstaller,
} from '@progress/kendo-scheduler-vue-wrapper'

Vue.config.productionTip = false

new Vue({
  router,
  components: {
    Scheduler,
  },
  render: h => h(App)
}).$mount('#app')
